#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

declare -x BBOSS_ENV_DISTRIB_BRANCH='develop'
declare -x INSTALL_SCT_PRODURL="https://test.pypi.org/simple/"
declare -x INSTALL_SCT_PRODVER="0.0.1.dev8"

declare -a distrib_lst=(
  centos7
  stretch
  buster
  xenial
  bionic
  cosmic
)

vagrant halt
for d in ${distrib_lst[@]}; do
  echo "[INFO ] Getting ${d} configuration"
  ouput="../config/${d}/"
  mkdir -p ${ouput}
  vagrant destroy -f ${d}
  vagrant up --no-provision ${d}
  vagrant ssh ${d} -c 'locale | sort' > "${ouput}locale.orig"
  vagrant ssh ${d} -c 'sudo cat /etc/ssh/sshd_config' > "${ouput}sshd_config.orig"
  vagrant provision ${d}
  vagrant ssh ${d} -c 'locale | sort' > "${ouput}locale"
  vagrant ssh ${d} -c 'sudo cat /etc/ssh/sshd_config' > "${ouput}sshd_config"
  vagrant ssh ${d} -c 'source "${HOME}/.bboss/venv/bin/activate" > /dev/null && pip freeze' > "${ouput}requirements.txt"
  vagrant halt ${d}
done