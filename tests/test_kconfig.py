'''
Created on Nov 17, 2018

@author: laurent
'''

import os
import kconfiglib


def main():
    os.environ['CONFIG_'] = 'BBOSS_'
    kconfig = kconfiglib.Kconfig("Kconfig.bboss")
    
    sym_lst = [(sym.name, sym.str_value) for sym in kconfig.unique_defined_syms]
    for sym in sorted(sym_lst):
        print("BBOSS_%s='%s'" % sym)

if __name__ == '__main__':
    main()
