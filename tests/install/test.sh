#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

declare -i retval=0

export BBOSS_ENV_DISTRIB_BRANCH="${BBOSS_ENV_DISTRIB_BRANCH:-${BUILD_SCT_INSTBRANCH}}"
export BBOSS_ENV_DISTRIB_BRANCH='develop'

if [[ ! -d "${HOME}/.bboss/venv" ]]; then
  exit 1
fi
source "${HOME}/.bboss/venv/bin/activate"
if [[ "${VIRTUAL_ENV}" ]]; then
  cd .bboss
  bboss generate -q install.bboss.org
  cd install
  bboss distclean
  cp /vagrant/defconfig .config
  bboss up
  bboss lsc
  bboss down
  cd ..
fi
